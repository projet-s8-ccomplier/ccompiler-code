#!/bin/bash

BASE=$(pwd)

PATH_FONCTIONAL_TEST="./test_unitaire/fonctionel/"
PATH_NONFONCTIONAL_TEST="./test_unitaire/non_fonctionel/"

PATH_RESULT_FONCTIONAL="./result_test/fonctionel/"
PATH_RESULT_NONFONCTIONAL="./result_test/non_fonctionel/"

cd $PATH_FONCTIONAL_TEST
MAXTESTF=$(ls -l | tail -n +3 | wc -l)
MAXTESTF="${MAXTESTF//[$'\t\r\n ']}"
cd $BASE
cd $PATH_NONFONCTIONAL_TEST 
MAXTESTNF=$(ls -l | tail -n +3 | wc -l)
MAXTESTNF="${MAXTESTNF//[$'\t\r\n ']}"

echo $MAXTESTF
echo $MAXTESTNF

NBTESTFONCTIONEL=$MAXTESTF
NBTESTNONFONCTIONEL=$MAXTESTNF

VERBOSE=0

cd $BASE

show_help()
{

echo "
        Usage: Compiler Test Environement [-v] [-f NumberFonctional] [-n NumberNonFonctional] [-h]

        -V     		    Activate Verbose mod - Show all tests results.
        -R NumberNonFonctional     Use this number of fonctional test.
        -S NumberFonctional        Use this number of non fonctional test.

        For Example: ./test_launcher -v -f 15 -n 5

        -h                         Help
"

exit 1
}

# Get Parameters
while getopts vhf:n: flag
do
    case "${flag}" in
        v) VERBOSE=1;;
        f) NBTESTFONCTIONEL=${OPTARG};;
        n) NBTESTNONFONCTIONEL=${OPTARG};;
        h) show_help;;
        *) show_help;;
    esac
done

# If parameters given is higher tha possible -> set to max
if [ "$NBTESTFONCTIONEL" -gt "$MAXTESTF" ]
then
	NBTESTFONCTIONEL=$MAXTESTF
fi

# If parameters given is higher tha possible -> set to max
if [ "$NBTESTNONFONCTIONEL" -gt "$MAXTESTNF" ]
then
	NBTESTNONFONCTIONEL=$MAXTESTNF
fi

# compilation of the app
make clean
make

# Fonctional test and redirection
for i in $(seq 0 1 "$NBTESTFONCTIONEL")
do
	if [ $i -lt 10 ]
	then
		./main "${PATH_FONCTIONAL_TEST}00${i}.c" "-v" > "${PATH_RESULT_FONCTIONAL}result_00${i}.txt"
	else
		./main "${PATH_FONCTIONAL_TEST}0${i}.c" "-v" > "${PATH_RESULT_FONCTIONAL}result_0${i}.txt"
	fi
done

# Non Fonctional test and redirection
for i in $(seq 0 1 $NBTESTNONFONCTIONEL)
do
	if [ $i -lt 10 ]
	then
		./main "${PATH_NONFONCTIONAL_TEST}00${i}.c" "-v" > "${PATH_RESULT_NONFONCTIONAL}result_00${i}.txt"
	else
		./main "${PATH_NONFONCTIONAL_TEST}0${i}.c" "-v" > "${PATH_RESULT_NONFONCTIONAL}result_0${i}.txt"
	fi	
done

echo -e " "

# If verbose flag is set
# Then show all the test results (fI)
if [ $VERBOSE -eq 1 ]
then
	echo "RESULT TEST FONCTIONEL :"
	for i in $(seq 0 1 "$NBTESTFONCTIONEL")
	do
		echo "TEST ${i} :"
		if [ $i -lt 10 ]
		then
			echo $(tail -1 "${PATH_RESULT_FONCTIONAL}result_00${i}.txt")
		else
			echo $(tail -1 "${PATH_RESULT_FONCTIONAL}result_0${i}.txt")
		fi
	done

	echo -e " "
	echo "RESULT TEST NON FONCTIONEL :"
	for i in $(seq 0 1 "$NBTESTNONFONCTIONEL")
	do
		echo "TEST ${i} :"
		if [ $i -lt 10 ]
		then
			echo $(tail -1 "${PATH_RESULT_NONFONCTIONAL}result_00${i}.txt")
		else
			echo $(tail -1 "${PATH_RESULT_NONFONCTIONAL}result_0${i}.txt")
		fi
	done
fi

echo -e "TEST DONE !"
