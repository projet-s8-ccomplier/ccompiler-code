CC=gcc
CFLAGS=-Werror -Wall
LDFLAGS=-g

SRC=$(wildcard *.c)
OBJ=$(patsubst %.c,%.o,$(SRC))
DEP=$(patsubst %.c,%.dep,$(SRC))

.PHONY:clean cleandep

all:main

main: $(OBJ)
	$(CC) $(LDFLAGS) -o $@ $^

($(OBJ):$(DEP))

%.o:%.c
	$(CC) $(CFLAGS) -c $<

%.dep:%.c
	$(CC) -MM $< > $@

clean:
	rm -f main *.o *.dep
cleandep:
	rm -f *.dep
cleansrc:
	rm -f main *.o

-include $(DEP)