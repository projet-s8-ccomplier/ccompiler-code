# MCPU emulator, see https://github.com/cpldcpu/MCPU
#   python asm.py <... | python emu.py
# optional arg is # of cycles to run
# show instruction trace if cycles < 1000

from __future__ import print_function
import sys

try:
    cycles = int(sys.argv[1]) # read the cycle number on the input
except:
    cycles = 20

mem = [int(x, 16) for x in sys.stdin.readlines()] # read the program
assert len(mem) == 64   # make sure that the program do not overlap

#registers
pc = 0  
acc = 0
cf = 0 #carry out

for _ in range(cycles):
    ir = mem[pc & 0x3F]                 # get the instruction code
    pc += 1                             # incr PC
    op, arg = ir >> 6, ir & 0x3F        # op = ir >> 6 and arg = ir & 0x3F 

    if cycles < 1000:                   
        print('pc,ir,acc,cf:', pc-1, hex(ir), acc, cf >> 8, sep='\t')   
            # 

    if op == 0: # NOR operation : Accu = Accu NOR mem[AAA AAA]
        acc = (acc | mem[arg]) ^ 0xFF # ^ = xor
    elif op == 1: # ADD operation : Accu = Accu + mem[AAA AAA], update carry
        acc += mem[arg]     # Calcul acc without carry
        cf = acc & 0x100    # Calcul carry out
        acc &= 0xFF         # calcul acc with carry
    elif op == 2: # Store operation : mem[AAA AAA] = Accu
        mem[arg] = acc      # Update the memory
    else: # Jump the PC to arg, and reset Carry
        if ir == 0xFF:
            print(acc)
        elif cf == 0:
            pc = arg
        cf = 0